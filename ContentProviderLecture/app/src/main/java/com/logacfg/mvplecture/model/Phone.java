package com.logacfg.mvplecture.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by Yegor on 8/12/17.
 */

public class Phone {

    public static final String TABLE_NAME = "Phones";
    public static final String ID = "_id";
    public static final String USER_ID = "user_id";
    public static final String PHONE = "phone";

    public static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_ID + " INTEGER NOT NULL, " +
            PHONE + " TEXT NOT NULL);";

    private long id;
    private long userId;
    private String phone;

    public Phone(Cursor cursor) {
        int index = cursor.getColumnIndex(ID);
        id = cursor.getLong(index);
        index = cursor.getColumnIndex(USER_ID);
        userId = cursor.getLong(index);
        index = cursor.getColumnIndex(PHONE);
        phone = cursor.getString(index);
    }

    public Phone(String phone) {
        this.phone = phone;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(USER_ID, userId);
        values.put(PHONE, phone);
        return values;
    }

    public String getPhone() {
        return phone;
    }
}
