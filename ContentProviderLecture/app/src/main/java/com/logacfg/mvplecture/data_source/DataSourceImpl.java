package com.logacfg.mvplecture.data_source;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;

import com.logacfg.mvplecture.model.Phone;
import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class DataSourceImpl implements DataSource {

    private static final String DOMEN = "http://xutpuk.pp.ua";
    private static final String USERS_URL = DOMEN + "/api/users.json";

    private static final String[] PROJECTION = {
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    };


    private static DataSource instance;

    private SQLiteOpenHelper helper;
    private Preferences storage;
    private NetworkHelper networkHelper;
    private ContentResolver contentResolver;

    public static DataSource getInstance(Context context) {
        if (instance == null) {
            instance = new DataSourceImpl(context);
        }
        return instance;
    }

    public DataSourceImpl(Context context) {
        helper = new DatabaseHelper(context);
        storage = new Preferences(context);
        networkHelper = NetworkHelper.getInstance(context);
        contentResolver = context.getContentResolver();
    }

    @Override
    public void insertUsers(List<User> users) {
        SQLiteDatabase db = helper.getWritableDatabase();
        for (User user : users) {
            long userId = db.insert(User.TABLE_NAME_V2, null, user.getContentValues());
            for (Phone phone : user.getPhones()) {
                phone.setUserId(userId);
                ContentValues contentValues = phone.getContentValues();
                db.insert(Phone.TABLE_NAME, null, contentValues);
            }
        }
        db.close();
    }

    @Override
    public long insertUser(User user) {
        SQLiteDatabase db = helper.getWritableDatabase();
        long id = db.insert(User.TABLE_NAME_V2, null, user.getContentValues());
        db.close();
        return id;
    }

    @Override
    public List<User> loadUsers() {
        List<User> users = null;
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(User.TABLE_NAME_V2, null, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            users = new ArrayList<>(cursor.getCount());
            cursor.moveToFirst();
            do {
                users.add(new User(cursor));
            } while (cursor.moveToNext());
            cursor.close();
            for (User user : users) {
                loadPhonesForUser(db, user);
            }
        }
        return users;
    }

    @Override
    public void loadUsers(Callback callback) {
        List<User> users = null;
        Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + "=?", new String[]{String.valueOf(1)}, null);
        if (cursor != null && cursor.getCount() > 0) {
            users = new ArrayList<>(cursor.getCount());
            cursor.moveToFirst();
            do {
                users.add(new User(cursor));
            } while (cursor.moveToNext());
            cursor.close();
            callback.onSuccess(users);
        }
//        networkHelper.sendRequest(USERS_URL, "GET", null, null, callback);
    }

    private void loadPhonesForUser(SQLiteDatabase db, User user) {
        long userId = user.getId();
        Cursor phonesCursor = db.query(Phone.TABLE_NAME, null, Phone.USER_ID + " = ?", new String[]{String.valueOf(userId)}, null, null, null);
        if (phonesCursor != null && phonesCursor.getCount() > 0) {
            phonesCursor.moveToFirst();
            do {
                user.addPhone(new Phone(phonesCursor));
            } while (phonesCursor.moveToNext());
            phonesCursor.close();
        }
    }

    @Override
    @Nullable
    public User getUserById(long id) {
        SQLiteDatabase db = helper.getReadableDatabase();
        User user = null;
        Cursor cursor = db.query(User.TABLE_NAME_V2, User.COLUMNS, User.ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            user = new User(cursor);
            cursor.close();
        }
        return user;
    }

    @Override
    public boolean isPreInited() {
        return storage.isPreInited();
    }

    @Override
    public void setPreInited(boolean preInited) {
        storage.setPreInited(preInited);
    }
}
