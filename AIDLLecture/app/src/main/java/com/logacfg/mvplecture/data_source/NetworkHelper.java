package com.logacfg.mvplecture.data_source;

import android.content.Context;
import android.os.Handler;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.logacfg.mvplecture.model.User;
import com.logacfg.mvplecture.model.Users;

import java.util.List;
import java.util.Map;

/**
 * Created by Yegor on 8/26/17.
 */

public class NetworkHelper {

    private static NetworkHelper instance;
    private Handler handler = new Handler();

    private RequestQueue queue;

    public static NetworkHelper getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkHelper(context);
        }
        return instance;
    }

    private NetworkHelper(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public void sendRequest(String strUrl, String method, Map<String, String> headers, Map<String, String> body, final DataSource.Callback callback) {
        BaseRequest<List<User>> request = new BaseRequest<List<User>>(Request.Method.GET, strUrl, new Response.Listener<List<User>>() {
            @Override
            public void onResponse(List<User> response) {
                if (callback != null) {
                    callback.onSuccess(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int i = 0;
            }
        }) {
            @Override
            protected List<User> parseResult(String response) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return parseUsers(response);
            }
        };
        queue.add(request);
    }

    private List<User> parseUsers(String response) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new User.UserDeserializer());
        return gsonBuilder.create().fromJson(response, Users.class);

    }
}
