package com.logacfg.mvplecture.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class User implements Parcelable {

    public static final String TABLE_NAME_V1 = "Users";
    public static final String TABLE_NAME_V2 = "Users_V2";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String SECOND_NAME = "second_name";
    public static final String PHONE_V1 = "phone";
    public static final String ADDRESS = "address";

    public static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME_V2 + " (" +
            User.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            User.NAME + " TEXT NOT NULL, " +
            User.SECOND_NAME + " TEXT, " +
            User.ADDRESS + " TEXT);";

    public static final String[] COLUMNS = {ID, NAME, SECOND_NAME, ADDRESS};

    private long id;
    private String name;
    @SerializedName("sname")
    private String secondName;
    private String address;
    private ArrayList<Phone> phones = new ArrayList<>();

    public User(String name, String secondName, String address) {
        this.name = name;
        this.secondName = secondName;
        this.address = address;
    }

    public User(Cursor cursor) {
        int index = cursor.getColumnIndex(ID);
        id = cursor.getLong(index);
        index = cursor.getColumnIndex(User.NAME);
        name = cursor.getString(index);
        index = cursor.getColumnIndex(User.SECOND_NAME);
        secondName = cursor.getString(index);
        index = cursor.getColumnIndex(User.ADDRESS);
    }

//    {
//        "id": 108,
//            "name": "asdfasdf",
//            "sname": "nbabab",
//            "last_update": "-0001-11-30T00:00:00-0500",
//            "phonesObj": [
//        "65756"
//    ],
//        "address": [
//        "fgjfg"
//    ]
//    }

    public User(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        id = jsonObject.get("id").getAsLong();
        name = jsonObject.get("name").getAsString();
        secondName = jsonObject.get("sname").getAsString();
        JsonArray jsonAddress = jsonObject.get("address").getAsJsonArray();
        if (jsonAddress.size() > 0) {
            address = jsonAddress.get(0).getAsString();
        }
        JsonArray jsonPhones = jsonObject.get("phones").getAsJsonArray();
        for (int i = 0; i < jsonPhones.size(); i++) {
            Phone phone = new Phone(jsonPhones.get(i).getAsString());
            phones.add(phone);
        }
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(User.NAME, name);
        values.put(User.SECOND_NAME, secondName);
        values.put(User.ADDRESS, address);
        return values;
    }

    public String getName() {
        return name + " " + secondName;
    }

    public String getAddress() {
        return address;
    }

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public List<Phone> getPhonesObj() {
        return phones;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return name + " " + secondName;
    }

    public static class UserDeserializer implements JsonDeserializer<User> {

        @Override
        public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return new User(json);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.secondName);
        dest.writeString(this.address);
        dest.writeTypedList(this.phones);
    }

    protected User(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.secondName = in.readString();
        this.address = in.readString();
        this.phones = in.createTypedArrayList(Phone.CREATOR);
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
