package com.logacfg.mvplecture.data_source;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.logacfg.mvplecture.model.Phone;
import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 7/29/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "MyDatabase";
    private static final int VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(User.CREATE_SQL);
        sqLiteDatabase.execSQL(Phone.CREATE_SQL);
     }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
        Cursor cursor = sqLiteDatabase.query(User.TABLE_NAME_V1, null, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                int index = cursor.getColumnIndex(User.NAME);
                String name = cursor.getString(index);
                index = cursor.getColumnIndex(User.SECOND_NAME);
                String secondName = cursor.getString(index);
                index = cursor.getColumnIndex(User.ADDRESS);
                String address = cursor.getString(index);
                index = cursor.getColumnIndex(User.PHONE_V1);
                String phone = cursor.getString(index);
                ContentValues userValues = new ContentValues();
                userValues.put(User.NAME, name);
                userValues.put(User.SECOND_NAME, secondName);
                userValues.put(User.ADDRESS, address);
                long newUserId = sqLiteDatabase.insert(User.TABLE_NAME_V2, null, userValues);
                ContentValues phoneValue = new ContentValues();
                phoneValue.put(Phone.USER_ID, newUserId);
                phoneValue.put(Phone.PHONE, phone);
                sqLiteDatabase.insert(Phone.TABLE_NAME, null, phoneValue);
            } while (cursor.moveToNext());
            cursor.close();
        }
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME_V1);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
