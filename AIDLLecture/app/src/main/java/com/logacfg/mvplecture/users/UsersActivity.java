package com.logacfg.mvplecture.users;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.logacfg.mvplecture.IMyAidlInterface;
import com.logacfg.mvplecture.LoadUsersService;
import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.User;

public class UsersActivity extends AppCompatActivity {

    public static final String LOAD_USERS_COMPLETE_ACTION = "com.logacfg.mvplecture.users.LOAD_USERS_COMPLETE_ACTION";

    public static Intent startIntent(Context context, Bundle extras) {
        Intent intent = new Intent(context, UsersActivity.class);
        intent.putExtras(extras);
        return intent;
    }

    private BroadcastReceiver receiver;
    private UsersContract.View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        startService(new Intent(this, LoadUsersService.class));
        UsersFragment fragment;
        if (savedInstanceState == null) {
            fragment = new UsersFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, fragment);
            fragmentTransaction.commit();
        } else {
            fragment = (UsersFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        }
        view = fragment;
        new UsersPresenter(this, fragment);
        bindService(new Intent(this, LoadUsersService.class), new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                IMyAidlInterface iMyAidlInterface = IMyAidlInterface.Stub.asInterface(iBinder);
                try {
                    User petya = iMyAidlInterface.getSomeUserWithName("Petya");
                    Toast.makeText(UsersActivity.this, petya.getName(), Toast.LENGTH_SHORT).show();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

//                LoadUsersService service = ((LoadUsersService.MyBinder) iBinder).getService();
//                service.loadUsers(new DataSource.Callback() {
//                    @Override
//                    public void onSuccess(List<User> users) {
//                        view.showUsers(users);
//                    }
//                });
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                int i = 0;
            }
        }, Service.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        receiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                ArrayList<User> users = intent.getParcelableArrayListExtra(LoadUsersService.USERS_LIST_EXTRA);
//                view.showUsers(users);
//            }
//        };
//        registerReceiver(receiver, new IntentFilter(LOAD_USERS_COMPLETE_ACTION));
    }

    @Override
    protected void onStop() {
//        unregisterReceiver(receiver);
        super.onStop();
    }
}
