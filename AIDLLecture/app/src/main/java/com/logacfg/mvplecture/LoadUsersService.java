package com.logacfg.mvplecture;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import com.logacfg.mvplecture.data_source.DataSource;
import com.logacfg.mvplecture.data_source.DataSourceFabric;
import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 9/2/17.
 */

public class LoadUsersService extends Service {

    public static final String USERS_LIST_EXTRA = "users_list_extra";

    private static final String LOG_TAG = "LoadUsersServiceTag";

    private DataSource dataSource;
    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Log.d("AIDL", "handleMessage");
                super.handleMessage(msg);
                startShowToastWithDelay();
            }
        };
        dataSource = DataSourceFabric.getInstance(this);
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "onStartCommand");
        return START_STICKY;
    }

    public void loadUsers(DataSource.Callback callback) {
        dataSource.loadUsers(callback);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }


    private void startShowToastWithDelay() {
        for (int i = 0; i < 10; i ++) {
            Log.d(LOG_TAG, "startShowToastWithDelay: " + i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                Log.d(LOG_TAG, "InterruptedException");
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return new IMyAidlInterface.Stub() {
            @Override
            public User getSomeUserWithName(String name) throws RemoteException {
                handler.sendEmptyMessage(200);
                Log.d(LOG_TAG, "getSomeUserWithName");
                return new User(name, "Vasil'ev", "Sumskaya");
            }
        };
    }

    public class MyBinder extends Binder {

        public LoadUsersService getService() {
            return LoadUsersService.this;
        }
    }
}
