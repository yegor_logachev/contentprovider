// IMyAidlInterface.aidl
package com.logacfg.mvplecture;

// Declare any non-default types here with import statements
import com.logacfg.mvplecture.model.User;

interface IMyAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
     User getSomeUserWithName(String name);
}
