package com.logacfg.mvplecture.data_source;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 9/9/17.
 */

public class UsersContentProvider extends ContentProvider {

    private static final String PROVIDER_LOG = "UsersContentProvider";

    private static final String AUTHORITY = "com.logacfg.mvplecture.provider";
    private static final String PATH = User.TABLE_NAME_V2;
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PATH);
    private static final int USERS_CODE = 1;
    private DatabaseHelper helper;
    private static UriMatcher matcher;

    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, PATH, USERS_CODE);
    }


    @Override
    public boolean onCreate() {
        helper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Log.d(PROVIDER_LOG, "query:" + uri.toString());
        Cursor cursor;
        int match = matcher.match(uri);
        switch (match) {
            case USERS_CODE:
                Log.d(PROVIDER_LOG, "Uri matched with code:" + match);
                SQLiteDatabase database = helper.getReadableDatabase();
                cursor = database.query(User.TABLE_NAME_V2, strings, s, strings1, null, null, s);
                Log.d(PROVIDER_LOG, "is cursor empty :" + (cursor == null || cursor.getCount() <= 0));
                break;

            default:
                Log.d(PROVIDER_LOG, "IllegalArgumentException was throw" + match);
                throw new IllegalArgumentException("This content provider doesn't support this URI: " + uri.toString());
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
