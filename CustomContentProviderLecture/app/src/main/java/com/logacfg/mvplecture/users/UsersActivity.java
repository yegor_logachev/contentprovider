package com.logacfg.mvplecture.users;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.logacfg.mvplecture.R;

public class UsersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        UsersFragment fragment;
        if (savedInstanceState == null) {
            fragment = new UsersFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, fragment);
            fragmentTransaction.commit();
        } else {
            fragment = (UsersFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        }
        new UsersPresenter(this, fragment);
    }
}
