package com.logacfg.mvplecture.add_user;

import android.content.Context;

import com.logacfg.mvplecture.data_source.DataSource;
import com.logacfg.mvplecture.data_source.DataSourceFabric;
import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 8/5/17.
 */

public class AddUserPresenter implements AddUserContract.Presenter {

    private AddUserContract.View view;
    private DataSource dataSource;

    public AddUserPresenter(Context context, AddUserContract.View view) {
        dataSource = DataSourceFabric.getInstance(context);
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void start() {}

    @Override
    public void addUser(User user) {
        long id = dataSource.insertUser(user);
        view.onUserAdded(id);
    }
}
