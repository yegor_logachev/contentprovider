package com.logacfg.mvplecture.add_user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.logacfg.mvplecture.R;

/**
 * Created by Yegor on 8/5/17.
 */

public class AddUserActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        AddUserFragment fragment;
        if (savedInstanceState == null) {
            fragment = new AddUserFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.container, fragment);
            fragmentTransaction.commit();
        } else {
            fragment = (AddUserFragment) getSupportFragmentManager().findFragmentById(R.id.container);
        }
        new AddUserPresenter(this, fragment);
    }
}
