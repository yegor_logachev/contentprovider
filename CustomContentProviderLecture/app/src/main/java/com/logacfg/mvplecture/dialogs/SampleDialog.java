package com.logacfg.mvplecture.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Yegor on 9/9/17.
 */

public class SampleDialog extends DialogFragment {

    public static DialogFragment newInstance() {
        return new SampleDialog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        builder.setTitle("Warning!");
////        builder.setMessage("Are sure that you wanna load users from database?");
////        ImageView imageView = new ImageView(getContext());
////        imageView.setImageResource(android.R.mipmap.sym_def_app_icon);
//        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1,
//                new String[]{"one", "two", "three"});
//        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                Toast.makeText(getContext(), i + " item was pressed", Toast.LENGTH_SHORT).show();
//            }
//        });
////        builder.setView(imageView);
////        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialogInterface, int i) {
////                Fragment targetFragment = getTargetFragment();
////                int targetRequestCode = getTargetRequestCode();
////                targetFragment.onActivityResult(targetRequestCode, Activity.RESULT_OK, null);
////            }
////        });
////        builder.setNegativeButton(android.R.string.cancel, null);
//        return builder.create();
//    }
}
