package com.logacfg.mvplecture.data_source;

import android.content.Context;
import android.os.Handler;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.logacfg.mvplecture.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Yegor on 8/26/17.
 */

public class NetworkHelper {

    private static NetworkHelper instance;
    private Handler handler = new Handler();

    private RequestQueue queue;

    public static NetworkHelper getInstance(Context context) {
        if (instance == null) {
            instance = new NetworkHelper(context);
        }
        return instance;
    }

    private NetworkHelper(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public void sendRequest(String strUrl, String method, Map<String, String> headers, Map<String, String> body, DataSource.Callback callback) {
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, strUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int i = 0;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int i = 0;
            }
        });
        queue.add(request);
    }

    private List<User> parseUsers(String response) throws JSONException {
        List<User> users = new ArrayList<>();
        JSONArray usersArray = new JSONArray(response);
        for (int i = 0; i < usersArray.length(); i++) {
            JSONObject userJsonObject = usersArray.optJSONObject(i);
            users.add(new User(userJsonObject));
        }
        return users;
    }

    private class RequestRunnable implements Runnable {

        private String strUrl;
        private String method;
        private Map<String, String> headers;
        private DataSource.Callback callback;

        public RequestRunnable(String strUrl, String method) {
            this.strUrl = strUrl;
            this.method = method;
        }

        public void setHeaders(Map<String, String> headers) {
            this.headers = headers;
        }

        public void setCallback(DataSource.Callback callback) {
            this.callback = callback;
        }

        @Override
        public void run() {
            HttpURLConnection connection = null;
            try {
                URL url = new URL(strUrl);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod(method);
                if (headers != null && !headers.isEmpty()) {
                    Set<String> keys = headers.keySet();
                    for (String key : keys) {
                        connection.setRequestProperty(key, headers.get(key));
                    }
                }
                connection.connect();
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = connection.getInputStream();
                    Scanner scanner = new Scanner(inputStream, "UTF-8");
                    StringBuffer buffer = new StringBuffer();
                    while (scanner.hasNext()) {
                        buffer.append(scanner.next());
                    }
                    final List<User> users = parseUsers(buffer.toString());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (callback != null) {
                                callback.onSuccess(users);
                            }
                        }
                    });
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null)
                    connection.disconnect();
            }
        }
    }
}
