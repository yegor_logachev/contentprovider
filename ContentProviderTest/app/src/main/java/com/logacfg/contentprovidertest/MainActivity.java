package com.logacfg.contentprovidertest;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "com.logacfg.mvplecture.provider";
    private static final String PATH = "Users_V2";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PATH);
    public static final String NAME = "name";
    private static final String[] COLUMNS = new String[] {NAME};

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        Cursor cursor = getContentResolver().query(CONTENT_URI, COLUMNS, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            List<String> names = new ArrayList<>();
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(NAME);
            do {
                names.add(cursor.getString(columnIndex));
            } while (cursor.moveToNext());
            cursor.close();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                    names);
            listView.setAdapter(adapter);
        }
    }
}
